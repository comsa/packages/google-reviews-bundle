<?php

declare(strict_types=1);

namespace Comsa\SuluGoogleReviewsBundle\Entity;

use Comsa\SuluGoogleReviewsBundle\Entity\Interfaces\CrudResource;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(repositoryClass: "Comsa\SuluGoogleReviewsBundle\Repository\SettingRepository"),
    Table(name: "comsa_gr_settings"),
    ExclusionPolicy("all")
]
class Setting implements CrudResource
{
    const RESOURCE_KEY = "comsa_gr_settings";

    #[
        Id,
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose
    ]
    private ?int $id;

    #[
        Column(type: Types::STRING, length: 255),
        Expose
    ]
    private string $title;

    #[
        Column(name: "`key`", type: Types::STRING, length: 255, unique: true),
        Expose
    ]
    private string $key;

    #[
        Column(type: Types::TEXT, length: 65000, nullable: true),
        Expose
    ]
    private ?string $value;

    public function __construct() {
        $this->id = null;
        $this->title = "";
        $this->key = "";
        $this->value = null;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     */
    public function setValue(?string $value): void
    {
        $this->value = $value;
    }


}
