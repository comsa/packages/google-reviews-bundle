<?php

declare(strict_types=1);

namespace Comsa\SuluGoogleReviewsBundle\Entity;

use Comsa\SuluGoogleReviewsBundle\Entity\Interfaces\CrudResource;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(repositoryClass: "Comsa\SuluGoogleReviewsBundle\Repository\GoogleReviewRepository"),
    Table(name: "comsa_gr_reviews"),
    ExclusionPolicy("all")
]
class GoogleReview implements CrudResource
{
    const RESOURCE_KEY = "comsa_gr_reviews";
    #[
        Id,
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose
    ]
    private ?int $id;

    #[
        Column(type: Types::STRING, length: 255, unique: true),
        Expose
    ]
    private string $reviewId;

    #[
        Column(type: Types::STRING, length: 255),
        Expose
    ]
    private string $starRating;

    #[
        Column(type: Types::STRING, length: 255),
        Expose
    ]
    private string $reviewer;

    #[
        Column(type: Types::TEXT, length: 65000, nullable: true),
        Expose
    ]
    private ?string $comment;

    #[
        Column(type: Types::STRING, length: 255, nullable: true),
        Expose
    ]
    private ?string $profilePhoto;

    #[
        Column(type: Types::STRING, length: 5, nullable: true),
        Expose
    ]
    private ?string $locale;

    #[
        Column(type: Types::DATETIME_MUTABLE),
        Expose
    ]
    private \DateTime $createdOn;

    #[
        Column(type: Types::DATETIME_MUTABLE),
        Expose
    ]
    private \DateTime $updatedOn;

    #[
        Column(type: Types::BOOLEAN),
        Expose
    ]
    private bool $isHidden;

    public function __construct()
    {
        $this->id = null;
        $this->reviewId = "";
        $this->starRating = "";
        $this->reviewer = "";
        $this->comment = null;
        $this->profilePhoto = null;
        $this->locale = "";
        $this->createdOn = new \DateTime();
        $this->updatedOn = new \DateTime();
        $this->isHidden = false;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getReviewId(): string {
        return $this->reviewId;
    }

    public function setReviewId(string $reviewId): self {
        $this->reviewId = $reviewId;

        return $this;
    }

    public function getStarRating(): string {
        return $this->starRating;
    }

    public function setStarRating(string $starRating): self {
        $this->starRating = $starRating;

        return $this;
    }

    public function getReviewer(): string {
        return $this->reviewer;
    }

    public function setReviewer(string $reviewer): self {
        $this->reviewer = $reviewer;

        return $this;
    }

    public function getComment(): ?string {
        return $this->comment;
    }

    public function setComment(?string $comment): self {
        $this->comment = $comment;

        return $this;
    }

    public function getProfilePhoto(): ?string {
        return $this->profilePhoto;
    }

    public function setProfilePhoto(?string $profilePhoto): self {
        $this->profilePhoto = $profilePhoto;

        return $this;
    }

    public function getLocale(): ?string {
        return $this->locale;
    }

    public function setLocale(?string $locale): self {
        $this->locale = $locale;

        return $this;
    }

    public function getCreatedOn(): \DateTime {
        return $this->createdOn;
    }

    public function setCreatedOn(\DateTime $createdOn): self {
        $this->createdOn = $createdOn;

        return $this;
    }

    public function getUpdatedOn(): \DateTime {
        return $this->updatedOn;
    }

    public function setUpdatedOn(\DateTime $updatedOn): self {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    public function isHidden(): bool {
        return $this->isHidden;
    }

    public function hide(): void {
        $this->isHidden = true;
    }

    public function show(): void {
        $this->isHidden = false;
    }
}
