<?php

declare(strict_types=1);

namespace Comsa\SuluGoogleReviewsBundle\Entity\Interfaces;

interface CrudResource {}
