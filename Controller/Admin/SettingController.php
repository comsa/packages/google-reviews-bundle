<?php

namespace Comsa\SuluGoogleReviewsBundle\Controller\Admin;

use Comsa\SuluGoogleReviewsBundle\Entity\Setting;
use Comsa\SuluGoogleReviewsBundle\Service\SettingService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SettingController extends AbstractRestController implements ClassResourceInterface
{
    private DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory;
    private FieldDescriptorFactoryInterface $fieldDescriptorFactory;
    private RestHelperInterface $restHelper;
    private SettingService $settingService;

    public function __construct(
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        SettingService $settingService
    ) {
        $this->doctrineListBuilderFactory = $doctrineListBuilderFactory;
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->settingService = $settingService;

        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request): Response
    {
        $listbuilder = $this->doctrineListBuilderFactory->create(Setting::class);
        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(Setting::RESOURCE_KEY);

        $this->restHelper->initializeListBuilder($listbuilder, $fieldDescriptors);

        $representation = new ListRepresentation(
            $listbuilder->execute(),
            "comsa_gr_settings",
            $request->get("_route"),
            $request->query->all(),
            $listbuilder->getCurrentPage(),
            $listbuilder->getLimit(),
            $listbuilder->count()
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id, Request $request): Response
    {
        $setting = $this->settingService->getRepository()->find($id);

        if (!$setting) {
            throw new NotFoundHttpException();
        }

        return $this->handleView($this->view($setting));
    }

    public function putAction(int $id, Request $request) : Response
    {
        /** @var Setting $setting */
        $setting = $this->settingService->getRepository()->find($id);

        if (!$setting) {
            throw new NotFoundHttpException();
        }

        $this->settingService->update($setting, $request->request->get("value"));

        return $this->handleView($this->view($setting));
    }
}
