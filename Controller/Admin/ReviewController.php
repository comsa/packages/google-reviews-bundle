<?php

namespace Comsa\SuluGoogleReviewsBundle\Controller\Admin;

use Comsa\SuluGoogleReviewsBundle\Entity\GoogleReview;
use Comsa\SuluGoogleReviewsBundle\Service\GoogleReviewsService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ReviewController extends AbstractRestController implements ClassResourceInterface
{
    private DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory;
    private FieldDescriptorFactoryInterface $fieldDescriptorFactory;
    private RestHelperInterface $restHelper;
    private GoogleReviewsService $service;

    public function __construct(
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        GoogleReviewsService $service
    )
    {
        $this->doctrineListBuilderFactory = $doctrineListBuilderFactory;
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->service = $service;

        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request): Response
    {
        $listbuilder = $this->doctrineListBuilderFactory->create(GoogleReview::class);
        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(GoogleReview::RESOURCE_KEY);
        $listbuilder->where($fieldDescriptors["isHidden"], false);
        $listbuilder->sort($fieldDescriptors["createdOn"], "DESC");

        $this->restHelper->initializeListBuilder($listbuilder, $fieldDescriptors);

        $representation = new ListRepresentation(
            $listbuilder->execute(),
            "comsa_gr_reviews",
            $request->get("_route"),
            $request->query->all(),
            $listbuilder->getCurrentPage(),
            $listbuilder->getLimit(),
            $listbuilder->count()
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int$id, Request $request): Response
    {
        $review = $this->service->getRepository()->find($id);

        if (!$review) {
            throw new NotFoundHttpException();
        }

        return $this->handleView($this->view($review));
    }

    public function deleteAction(int $id, Request $request): Response
    {
        /** @var GoogleReview $review */
        $review = $this->service->getRepository()->find($id);

        if (!$review) {
            throw new NotFoundHttpException();
        }

        $this->service->hide($review);

        return $this->handleView($this->view());
    }
}
