<?php

declare(strict_types=1);

namespace Comsa\SuluGoogleReviewsBundle\Repository;

use Comsa\SuluGoogleReviewsBundle\Entity\Setting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class SettingRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Setting::class);
    }
}
