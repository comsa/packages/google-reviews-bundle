<?php

namespace Comsa\SuluGoogleReviewsBundle\Repository;

use Comsa\SuluGoogleReviewsBundle\Entity\GoogleReview;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GoogleReview|null    find($id, $lockMode = null, $lockVersion = null)
 * @method GoogleReview|null    findOneBy(array $criteria, ?array $orderBy = null)
 * @method GoogleReview[]       findAll()
 * @method GoogleReview[]       findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
class GoogleReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, GoogleReview::class);
    }

    const RATINGS = [
        1 => 'ONE',
        2 => 'TWO',
        3 => 'THREE',
        4 => 'FOUR',
        5 => 'FIVE'
    ];

    public function findAmount(int $amount, string $locale, int $ratingFrom)
    {
        $buildRatingSearchQuery = [];
        for ($i = $ratingFrom; $i <= count(static::RATINGS); $i++) {
            $buildRatingSearchQuery[] = static::RATINGS[$i];
        }

        return $this->createQueryBuilder("gr")
            ->where("gr.isHidden = 0")
            ->andWhere("gr.locale = :locale")
            ->andWhere('gr.starRating IN (:ratings)')
            ->setParameter("locale", $locale)
            ->setParameter('ratings', $buildRatingSearchQuery)
            ->orderBy("gr.createdOn", "DESC")
            ->setMaxResults($amount)
            ->getQuery()
            ->getResult();
    }
}
