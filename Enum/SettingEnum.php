<?php

declare(strict_types=1);

namespace Comsa\SuluGoogleReviewsBundle\Enum;

/**
 * Class SettingEnum
 * @package
 */
class SettingEnum
{

    const TITLES_ACCOUNT_ID = "Google Account ID";
    const KEYS_ACCOUNT_ID = "accountId";

    const TITLES_LOCATION_ID = "Google Location ID";
    const KEYS_LOCATION_ID = "locationId";

    const TITLES_CLIENT_URL = "Google Client URL";
    const KEYS_CLIENT_URL = "clientUrl";

    const TITLES_DOMAIN = "Domain";
    const KEYS_DOMAIN = "domain";
}
