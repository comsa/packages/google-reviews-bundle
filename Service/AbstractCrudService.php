<?php

declare(strict_types=1);

namespace Comsa\SuluGoogleReviewsBundle\Service;

use Comsa\SuluGoogleReviewsBundle\Entity\Interfaces\CrudResource;
use Comsa\SuluGoogleReviewsBundle\Exception\InvalidCrudResourceException;
use Comsa\SuluGoogleReviewsBundle\Service\Interfaces\BaseOrmManagementInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractCrudService implements BaseOrmManagementInterface {
    private EntityManagerInterface $entityManager;
    private ServiceEntityRepository $repository;
    private string $expectedClass;

    public function __construct(EntityManagerInterface $entityManager, ServiceEntityRepository $repository, string $expectedClass) {
        $this->entityManager = $entityManager;
        $this->repository = $repository;
        $this->expectedClass = $expectedClass;
    }

    public function getRepository(): ServiceEntityRepository {
        return $this->repository;
    }

    public function save(CrudResource $entity): void {
        $this->validateEntity($entity);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    public function delete(CrudResource $entity): void {
        $this->validateEntity($entity);
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    public function validateEntity(CrudResource $entity): void {
        if (!is_a($entity, $this->expectedClass)) {
            throw new InvalidCrudResourceException($entity::class, $this->expectedClass);
        }
    }
}
