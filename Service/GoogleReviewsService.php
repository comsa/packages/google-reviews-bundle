<?php

declare(strict_types=1);

namespace Comsa\SuluGoogleReviewsBundle\Service;

use Comsa\SuluGoogleReviewsBundle\Entity\GoogleReview;
use Comsa\SuluGoogleReviewsBundle\Entity\Setting;
use Comsa\SuluGoogleReviewsBundle\Factory\GoogleReviewFactory;
use Comsa\SuluGoogleReviewsBundle\Repository\GoogleReviewRepository;
use Comsa\SuluGoogleReviewsBundle\Service\Interfaces\BaseCrudServiceInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class GoogleReviewsService extends AbstractCrudService implements BaseCrudServiceInterface {
    public function __construct(EntityManagerInterface $entityManager, GoogleReviewRepository $repository) {
        parent::__construct($entityManager, $repository, GoogleReview::class);
    }

    public function create(array $data): void {
        $entity = GoogleReviewFactory::create(
            $data["externalId"],
            $data["starRating"],
            $data["reviewer"],
            $data["comment"],
            $data["profilePhotoUrl"],
            $data["locale"],
            $data["createdOn"],
            $data["updatedOn"]
        );

        $this->save($entity);
    }

    public function hide(GoogleReview $googleReview) {
        $googleReview->hide();
        $this->save($googleReview);
    }
}
