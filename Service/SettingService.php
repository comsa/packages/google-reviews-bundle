<?php

declare(strict_types=1);

namespace Comsa\SuluGoogleReviewsBundle\Service;

use Comsa\SuluGoogleReviewsBundle\Entity\Setting;
use Comsa\SuluGoogleReviewsBundle\Enum\SettingEnum;
use Comsa\SuluGoogleReviewsBundle\Repository\SettingRepository;
use Doctrine\ORM\EntityManagerInterface;

class SettingService extends AbstractCrudService {
    public function __construct(EntityManagerInterface $entityManager, SettingRepository $repository) {
        parent::__construct($entityManager, $repository, Setting::class);
    }

    public function getAccount(): Setting {
        return $this->getSetting(SettingEnum::KEYS_ACCOUNT_ID);
    }

    public function getLocation(): Setting {
        return $this->getSetting(SettingEnum::KEYS_LOCATION_ID);
    }

    public function getClient(): Setting {
        return $this->getSetting(SettingEnum::KEYS_CLIENT_URL);
    }

    public function getDomain(): Setting {
        return $this->getSetting(SettingEnum::KEYS_DOMAIN);
    }

    private function getSetting(string $key): Setting {
        return $this->getRepository()->findOneBy([
            "key" => $key
        ]);
    }

    public function update(Setting $setting, string $value) {
        $setting->setValue($value);
        $this->save($setting);
    }
}
