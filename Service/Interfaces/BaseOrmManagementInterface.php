<?php

declare(strict_types=1);

namespace Comsa\SuluGoogleReviewsBundle\Service\Interfaces;

use Comsa\SuluGoogleReviewsBundle\Entity\Interfaces\CrudResource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

interface BaseOrmManagementInterface {
    public function getRepository(): ServiceEntityRepository;
    public function save(CrudResource $entity): void;
    public function delete(CrudResource $entity): void;
    public function validateEntity(CrudResource $entity): void;
}
