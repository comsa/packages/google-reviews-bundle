<?php

declare(strict_types=1);

namespace Comsa\SuluGoogleReviewsBundle\Command;

use Comsa\SuluGoogleReviewsBundle\Entity\GoogleReview;
use Comsa\SuluGoogleReviewsBundle\Entity\Setting;
use Comsa\SuluGoogleReviewsBundle\Enum\SettingEnum;
use Comsa\SuluGoogleReviewsBundle\Service\GoogleReviewsService;
use Comsa\SuluGoogleReviewsBundle\Service\SettingService;
use Doctrine\Persistence\ObjectRepository;
use LanguageDetection\Language;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[
    AsCommand(
        name: "comsa:import:google-reviews",
        description: "Fetches google reviews"
    )
]
class GetGoogleReviewsCommand extends Command {

    private HttpClientInterface $client;
    private GoogleReviewsService $reviewService;
    private SettingService $settingService;

    public function __construct(HttpClientInterface $client, GoogleReviewsService $reviewService, SettingService $settingService) {
        $this->client = $client;
        $this->reviewService = $reviewService;
        $this->settingService = $settingService;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $io = new SymfonyStyle($input, $output);
        $startTime = new \DateTime();
        $io->title("Import started at {$startTime->format('d-m-Y H:i:s')}");

        $accountId = $this->settingService->getAccount()->getValue();
        $locationId = $this->settingService->getLocation()->getValue();
        $clientUrl = $this->settingService->getClient()->getValue();
        $domain = $this->settingService->getDomain()->getValue();

        $data = $this->client->request(
          "POST",
          $clientUrl, [
              "headers" => [
                "Accept" => "application/json",
                "domain" => $domain
              ],
            ]
        );

        $data = $data->toArray();

        $reviews = $data["reviews"];
        $progressBar = new ProgressBar($output, count($reviews));

        $progressBar->start();
        foreach ($reviews as $review) {
            if (!$this->reviewService->getRepository()->findOneBy(["reviewId" => $review["reviewId"]])) {
                $this->reviewService->create([
                    "externalId" => $review["reviewId"],
                    "starRating" => $review["starRating"],
                    "reviewer" => $review["reviewer"]["displayName"],
                    "comment" => $this->filterComment($review["comment"]),
                    "profilePhotoUrl" => $review["reviewer"]["profilePhotoUrl"],
                    "locale" => $this->getLocale($this->filterComment($review["comment"])),
                    "createdOn" => new \DateTime($review["createTime"]),
                    "updatedOn" => new \DateTime($review["updateTime"])
                ]);

                $progressBar->advance();
            }
        }

        $progressBar->finish();

        $finishTime = new \DateTime();
        $io->newLine();
        $io->title("Import completed at {$finishTime->format('d-m-Y H:i:s')}");

        return Command::SUCCESS;
    }

    public function getLocale(?string $comment) {
        if (!$comment) {
            return null;
        }

        $detector = new Language();
        $language = $detector->detect($comment)->bestResults()->close();
        $locale = array_keys($language);
        if (empty($locale)) {
            return null;
        }

        return $locale[0];
    }

    public function filterComment(?string $comment) {
        if (!$comment) {
            return;
        }

        $translatedByGoogle = strpos($comment, '(Translated by Google)');
        if ($translatedByGoogle === false) {
            return;
        }

        //-- It's translated by Google, fetch the original
        $original = strpos($comment, '(Original)');
        if ($original !== false) {
            $original += strlen('(Original)');
        }

        if ($original === false) {
            //-- Take everything in front of translated by google
            $comment = trim(substr($comment, 0, $translatedByGoogle));
        } else {
            $comment = trim(substr($comment, $original));
        }

        return $comment;
    }
}
