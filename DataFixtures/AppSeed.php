<?php

namespace Comsa\SuluGoogleReviewsBundle\DataFixtures;

use Comsa\SuluGoogleReviewsBundle\Enum\SettingEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Comsa\SuluGoogleReviewsBundle\Entity\Setting;
use Ramsey\Collection\Set;

class AppSeed extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $manager->persist($this->loadSettingAccountId());
        $manager->persist($this->loadSettingLocationId());
        $manager->persist($this->loadSettingClientUrl());
        $manager->persist($this->loadDomainSetting());

        $manager->flush();
    }

    private function loadSettingAccountId(): Setting
    {
        $setting = new Setting();
        $setting->setTitle(SettingEnum::TITLES_ACCOUNT_ID);
        $setting->setKey(SettingEnum::KEYS_ACCOUNT_ID);
        $setting->setValue(null);

        return $setting;
    }

    private function loadSettingLocationId(): Setting
    {
        $setting = new Setting();
        $setting->setTitle(SettingEnum::TITLES_LOCATION_ID);
        $setting->setKey(SettingEnum::KEYS_LOCATION_ID);
        $setting->setValue(null);

        return $setting;
    }

    private function loadSettingClientUrl(): Setting
    {
        $setting = new Setting();
        $setting->setTitle(SettingEnum::TITLES_CLIENT_URL);
        $setting->setKey(SettingEnum::KEYS_CLIENT_URL);
        $setting->setValue(null);

        return $setting;
    }

    private function loadDomainSetting(): Setting
    {
        $setting = new Setting();
        $setting->setTitle(SettingEnum::TITLES_DOMAIN);
        $setting->setKey(SettingEnum::KEYS_DOMAIN);
        $setting->setValue(null);

        return $setting;
    }
}
