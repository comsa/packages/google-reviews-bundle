<?php

declare(strict_types=1);

namespace Comsa\SuluGoogleReviewsBundle\Factory;

use Comsa\SuluGoogleReviewsBundle\Entity\Setting;

/**
 * Factory Setting
 * @package Comsa\SuluGoogleReviewsBundle\Factory
 */
final class SettingFactory {
    public static function create(string $title, string $key, ?string $value): Setting {
        return (new Setting())
            ->setTitle($title)
            ->setKey($key)
            ->setValue($value)
        ;
    }
}
