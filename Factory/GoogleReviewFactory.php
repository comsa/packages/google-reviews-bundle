<?php

declare(strict_types=1);

namespace Comsa\SuluGoogleReviewsBundle\Factory;

use Comsa\SuluGoogleReviewsBundle\Entity\GoogleReview;

/**
 * Factory GoogleReview
 * @package Comsa\SuluGoogleReviewsBundle\Factory
 */
final class GoogleReviewFactory {
    public static function create(
        string $reviewId,
        string $starRating,
        string $reviewer,
        ?string $comment,
        ?string $profilePhoto,
        ?string $locale,
        \DateTime $createTime,
        \DateTime $updateTime
    ): GoogleReview {
        return (new GoogleReview())
            ->setReviewId($reviewId)
            ->setStarRating($starRating)
            ->setReviewer($reviewer)
            ->setComment($comment)
            ->setProfilePhoto($profilePhoto)
            ->setLocale($locale)
            ->setCreatedOn($createTime)
            ->setUpdatedOn($updateTime)
        ;
    }
}
