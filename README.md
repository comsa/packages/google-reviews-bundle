# Installation
```bash
composer require comsa/facebook-bundle
```

# Setup

Read the following documentation and check if all requirements are met:
1. https://developers.google.com/my-business/content/prereqs
2. https://developers.google.com/my-business/content/basic-setup
3. https://developers.google.com/my-business/content/review-data


Execute:
```bash
php bin/console doctrine:schema:update -f
```

Add the following code to src/DataFixtures/AppFixtures.php:
```php
<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Comsa\SuluGoogleReviewsBundle\DataFixtures\AppSeed as SuluGoogleReviewsAppSeed;

class AppFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
    }

    public function getDependencies(): array
    {
        return [
            SuluGoogleReviewsAppSeed::class
        ];
    }
}
```

Execute:
```bash
php bin/console doctrine:fixtures:load --append
```

# Configure Sulu
in config/routes_admin.yaml:
```yaml
sulu_comsa_google_reviews_admin:
  type: rest
  resource: "@SuluGoogleReviewsBundle/Resources/config/routes/admin.yaml"
  prefix: /admin/google/api
```

Google Reviews Block Template:
```xml
<?xml version="1.0" ?>
<type name="google_reviews" xmlns="http://schemas.sulu.io/template/template"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xi="http://www.w3.org/2001/XInclude"
      xsi:schemaLocation="http://schemas.sulu.io/template/template http://schemas.sulu.io/template/template-1.0.xsd">
    <meta>
        <title lang="en">Reviews</title>
        <title lang="nl">Recensies</title>
    </meta>
    <properties>
        <property name="loadType" type="single_select">
            <meta>
                <title lang="en">How do you want to load the reviews?</title>
                <title lang="nl">Hoe wil je de recensies ophalen?</title>
            </meta>

            <params>
                <param name="default_value" value="dynamically"/>

                <param name="values" type="collection">
                    <param name="dynamic">
                        <meta>
                            <title lang="en">Dynamically</title>
                            <title lang="nl">Dynamisch</title>
                        </meta>
                    </param>

                    <param name="static">
                        <meta>
                            <title lang="en">Static reviews</title>
                            <title lang="nl">Vaste recensies</title>
                        </meta>
                    </param>
                </param>
            </params>
        </property>

        <property name="review_selection" type="review_selection" visibleCondition="__parent.loadType == 'static'">
            <meta>
                <title lang="en">Reviews</title>
                <title lang="nl">Recensies</title>
            </meta>
        </property>

        <property name="min_rating" type="single_select" colspan="6" visibleCondition="__parent.loadType == 'dynamic'">
            <meta>
                <title lang="en">Minimum Rating</title>
                <title lang="nl">Minimum Beroordeling</title>
            </meta>

            <params>
                <param name="default_value" value="THREE"/>

                <param name="values" type="collection">
                    <param name="ONE">
                        <meta>
                            <title>1</title>
                        </meta>
                    </param>
                    <param name="TWO">
                        <meta>
                            <title>2</title>
                        </meta>
                    </param>
                    <param name="THREE">
                        <meta>
                            <title>3</title>
                        </meta>
                    </param>
                    <param name="FOUR">
                        <meta>
                            <title>4</title>
                        </meta>
                    </param>
                    <param name="FIVE">
                        <meta>
                            <title>5</title>
                        </meta>
                    </param>
                </param>
            </params>
        </property>
        <property name="amount" type="number" colspan="6" visibleCondition="__parent.loadType == 'dynamic'">
            <meta>
                <title lang="en">Amount of reviews</title>
                <title lang="nl">Aantal recensies</title>
            </meta>
            <params>
                <param name="min" value="0" />
            </params>
        </property>

        <xi:include href="includes/width.xml"/>
    </properties>
</type>
```

# Settings

Settings can be found in the Sulu admin environment. There you can enter your account, location and client url.

# Frontend
Google Reviews Template example:
```php
{% if block.loadType == "static" %}
  {% set reviews = block.review_selection %}
{% else %}
  {% set reviews = parse_review_widget(block.amount, app.request.locale, block.min_rating) %}
{% endif %}

<div class="google-reviews">
  <div class="row">
    {% for review in reviews %}
      <div class="col-md-4 my-3">
        <div class="card" style="height: 25rem;">
          <div class="card-title text-center w-100 pt-3">
            {% if review.profilePhoto %}
              <img src="{{ review.profilePhoto }}" alt="{{ review.reviewer }}" title="{{ review.reviewer }}" class="card-img-top img-fluid w-25">
            {% endif %}
          </div>
          <div class="card-body text-center">
            <h3>
              {{ review.reviewer }}
            </h3>
            <div class="d-flex w-100 justify-content-center">
              {% set imgs = get_star_rating(review.starRating) %}
              {% for img in imgs %}
                {{ img|raw }}
              {% endfor %}
            </div>
            <div class="mt-3">
              {% if review.comment %}
                {{ review.comment }}
              {% endif %}
            </div>
          </div>
        </div>
      </div>
    {% endfor %}
  </div>
</div>
```

# Commands

Get reviews:
```bash
php bin/console comsa:import:google-reviews
```


