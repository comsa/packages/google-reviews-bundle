<?php

namespace Comsa\SuluGoogleReviewsBundle\Twig;

use Comsa\SuluGoogleReviewsBundle\Entity\GoogleReview;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SuluGoogleReviewsExtension extends AbstractExtension
{
    private TranslatorInterface $translator;
    private EntityManagerInterface $entityManager;

    public function __construct(TranslatorInterface $translator, EntityManagerInterface $entityManager)
    {
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }


    public function getFunctions()
    {
        return [
            new TwigFunction("parse_review_widget", [$this, "parseReviewsWidget"]),
            new TwigFunction("get_star_rating", [$this, "getStarRatingImg"])
        ];
    }

    public function parseReviewsWidget(int $amount, string $locale, string $rating)
    {
        $rating= match ($rating) {
            "ONE" => 1,
            "TWO" => 2,
            "THREE" => 3,
            "FOUR" => 4,
            "FIVE" => 5
        };

        return $this->entityManager->getRepository(GoogleReview::class)->findAmount($amount, $locale, $rating);
    }

    public function getStarRatingImg(string $rating)
    {
        $amount= match ($rating) {
          "ONE" => 1,
          "TWO" => 2,
          "THREE" => 3,
          "FOUR" => 4,
          "FIVE" => 5
        };

        $imgs = [];

        for ($x = 1; $x <= $amount; $x++) {
            $imgs[] = '<i class="fas fa-star text-warning"></i>';
        }

        return $imgs;
    }
}
