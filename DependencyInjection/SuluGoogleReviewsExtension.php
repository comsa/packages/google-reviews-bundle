<?php

namespace Comsa\SuluGoogleReviewsBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\FileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class SuluGoogleReviewsExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . "/../Resources/config"));
        $loader->load("services.yaml");
    }

    public function prepend(ContainerBuilder $container)
    {
        if ($container->hasExtension("fos_js_routing")) {
            $container->prependExtensionConfig(
                "fos_js_routing",
                [
                    "routes_to_expose" => [
                        "comsa_sulu_google_reviews.get_reviews",
                        "comsa_sulu_google_reviews.get_review",
                        "comsa_sulu_google_reviews.get_settings",
                        "comsa_sulu_google_reviews.get_setting"
                    ]
                ]
            );
        }

        if ($container->hasExtension("sulu_admin"))
        {
            $container->prependExtensionConfig(
                "sulu_admin",
                [
                    "lists" => [
                        "directories" => [
                            __DIR__ . "/../Resources/config/lists"
                        ]
                    ],
                    "forms" => [
                        "directories" => [
                            __DIR__ . "/../Resources/config/forms"
                        ]
                    ],
                    "resources" => [
                        "comsa_gr_reviews" => [
                            "routes" => [
                                "list" => "comsa_sulu_google_reviews.get_reviews",
                                "detail" => "comsa_sulu_google_reviews.get_review"
                            ]
                        ],
                        "comsa_gr_settings" => [
                            "routes" => [
                                "list" => "comsa_sulu_google_reviews.get_settings",
                                "detail" => "comsa_sulu_google_reviews.get_setting"
                            ]
                        ]
                    ],
                    "field_type_options" => [
                        "selection" => [
                            "review_selection" => [
                                "default_type" => "list_overlay",
                                "resource_key" => "comsa_gr_reviews",
                                "types" => [
                                    "list_overlay" => [
                                        "adapter" => "table",
                                        "list_key" => "comsa_gr_reviews",
                                        "display_properties" => [
                                            "reviewer",
                                            "comment"
                                        ],
                                        "icon" => "su-list-ul",
                                        "label" => "Google Reviews",
                                        "overlay_title" => "Select your Google Reviews"
                                    ]
                                ]
                            ]
                        ],
                    ]
                ]
            );
        }
    }
}
