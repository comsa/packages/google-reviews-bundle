<?php

namespace Comsa\SuluGoogleReviewsBundle\Content\Types;

use Comsa\SuluGoogleReviewsBundle\Entity\GoogleReview;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Component\Content\ComplexContentType;
use Doctrine\ORM\Mapping\Entity;
use Jackalope\Node;
USE PHPCR\NodeInterface;
use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\PreResolvableContentTypeInterface;

class ReviewSelection extends ComplexContentType
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function read(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey)
    {
        $reviewIds = $node->getPropertyValueWithDefault($property->getName(), []);
        $property->setValue($reviewIds);
    }

    public function write(NodeInterface $node, PropertyInterface $property, $userId, $webspaceKey, $languageCode, $segmentKey)
    {
        $reviewIds = [];
        $value = $property->getValue();

        if (!$value) {
            $node->setProperty($property->getName(), null);
            return;
        }

        foreach ($value as $review) {
            if (is_numeric($review)) {
                $reviewIds[] = $review;
            } else {
                $reviewIds[] = $review["id"];
            }
        }

        $node->setProperty($property->getName(), $reviewIds);
    }

    public function remove(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey)
    {
        if ($node->hasProperty($property->getName())) {
            $property = $node->getProperty($property->getName());
            $property->remove();
        }
    }

    public function getContentData(PropertyInterface $property)
    {
        $reviews = $property->getValue();

        if (!$reviews) {
            return;
        }

        foreach ($reviews as &$review) {
            $review = $this->entityManager->getRepository(GoogleReview::class)->find($review);
        }

        return $reviews;
    }
}
