<?php

declare(strict_types=1);

namespace Comsa\SuluGoogleReviewsBundle\Exception;

use Throwable;

class InvalidCrudResourceException extends \Exception {
    public function __construct(string $givenClass, string $expectedClass) {
        $message = sprintf(
            "Unexpected class resource given. Expected '%s', got '%s'",
            $expectedClass,
            $givenClass
        );

        parent::__construct($message, 500);
    }
}
