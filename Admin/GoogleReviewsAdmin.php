<?php

namespace Comsa\SuluGoogleReviewsBundle\Admin;

use Comsa\SuluGoogleReviewsBundle\Entity\GoogleReview;
use Comsa\SuluGoogleReviewsBundle\Entity\Setting;
use Sulu\Bundle\AdminBundle\Admin\Admin;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItem;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItemCollection;
use Sulu\Bundle\AdminBundle\Admin\View\ToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewCollection;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;

class GoogleReviewsAdmin extends Admin
{
    //-- Reviews
    const GOOGLE_REVIEW_LIST_VIEW = "comsa_sulu_google_reviews.reviews_list";
    const GOOGLE_REVIEWS_LIST_KEY = "comsa_gr_reviews";
    const GOOGLE_REVIEWS_EDIT_VIEW = "comsa_sulu_google_reviews.review_detail";
    const GOOGLE_REVIEWS_FORM_KEY = "comsa_gr_review_details";

    //-- Settings
    const SETTING_LIST_VIEW = "comsa_sulu_google_reviews.settings_list";
    const SETTING_LIST_KEY = "comsa_gr_settings";
    const SETTING_EDIT_VIEW = "comsa_sulu_google_reviews.setting_detail";
    const SETTING_FORM_KEY = "comsa_gr_setting_details";

    private ViewBuilderFactoryInterface $viewBuilderFactory;
    private WebspaceManagerInterface $webspaceManager;

    public function __construct(ViewBuilderFactoryInterface $viewBuilderFactory, WebspaceManagerInterface $webspaceManager)
    {
        $this->viewBuilderFactory = $viewBuilderFactory;
        $this->webspaceManager = $webspaceManager;
    }

    public function configureNavigationItems(NavigationItemCollection $navigationItemCollection): void
    {
        $module = new NavigationItem("comsa_sulu_google_reviews.reviews_module");
        $module->setIcon("su-check-circle");

        $reviewsItem = new NavigationItem("comsa_sulu_google_reviews.reviews");
        $reviewsItem->setView(self::GOOGLE_REVIEW_LIST_VIEW);

        $settingsItem = new NavigationItem("comsa_sulu_google_reviews.settings");
        $settingsItem->setView(self::SETTING_LIST_VIEW);

        $module->addChild($reviewsItem);
        $module->addChild($settingsItem);

        $navigationItemCollection->add($module);
    }

    public function configureReviewViews(ViewCollection $viewCollection): void
    {
        $reviewListView = $this->viewBuilderFactory->createListViewBuilder(
            self::GOOGLE_REVIEW_LIST_VIEW,
            "/google-reviews/reviews"
        )
            ->setResourceKey(GoogleReview::RESOURCE_KEY)
            ->setListKey(self::GOOGLE_REVIEWS_LIST_KEY)
            ->setTitle("comsa_sulu_google_reviews.reviews")
            ->addListAdapters(["table"])
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.delete")
            ])
            ->setEditView(self::GOOGLE_REVIEWS_EDIT_VIEW);

        $viewCollection->add($reviewListView);

        $editReviewResourceTab = $this->viewBuilderFactory->createResourceTabViewBuilder(
            self::GOOGLE_REVIEWS_EDIT_VIEW,
            "/google-reviews/reviews/:id"
        )
            ->setResourceKey(GoogleReview::RESOURCE_KEY)
            ->setBackView(self::GOOGLE_REVIEW_LIST_VIEW)
            ->setTitleProperty("externalId");

        $viewCollection->add($editReviewResourceTab);

        $editReviewView = $this->viewBuilderFactory->createFormViewBuilder(
            self::GOOGLE_REVIEWS_EDIT_VIEW . ".details",
            "/google-reviews/details"
        )
            ->setResourceKey(GoogleReview::RESOURCE_KEY)
            ->setFormKey(self::GOOGLE_REVIEWS_FORM_KEY)
            ->setTabTitle("sulu_admin.details")
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.delete")
            ])
            ->setParent(self::GOOGLE_REVIEWS_EDIT_VIEW);

        $viewCollection->add($editReviewView);
    }

    public function configureSettingViews(ViewCollection $viewCollection): void
    {
        $settingListView = $this->viewBuilderFactory->createListViewBuilder(
            self::SETTING_LIST_VIEW,
            "/google-reviews/settings"
        )
            ->setResourceKey(Setting::RESOURCE_KEY)
            ->setListKey(self::SETTING_LIST_KEY)
            ->setTitle("comsa_sulu_google_reviews.settings")
            ->addListAdapters(["table"])
            ->setEditView(self::SETTING_EDIT_VIEW);

        $viewCollection->add($settingListView);

        $editSettingResourceTab = $this->viewBuilderFactory->createResourceTabViewBuilder(
            self::SETTING_EDIT_VIEW,
            "/google_reviews/settings/:id"
        )
            ->setResourceKey(Setting::RESOURCE_KEY)
            ->setBackView(self::SETTING_LIST_VIEW)
            ->setTitleProperty("title");

        $viewCollection->add($editSettingResourceTab);

        $editSettingView = $this->viewBuilderFactory->createFormViewBuilder(
            self::SETTING_EDIT_VIEW . ".details",
            "/google-reviews/details"
        )
            ->setResourceKey(Setting::RESOURCE_KEY)
            ->setFormKey(self::SETTING_FORM_KEY)
            ->setTabTitle("sulu_admin.details")
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.save")
            ])
            ->setParent(self::SETTING_EDIT_VIEW);

        $viewCollection->add($editSettingView);
    }

    public function configureViews(ViewCollection $viewCollection): void
    {
        $this->configureReviewViews($viewCollection);
        $this->configureSettingViews($viewCollection);
    }
}
